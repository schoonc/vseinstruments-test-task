/*
 * Copyright (c) 2020. Alexander Sorokin
 */

export class SortBy {
  constructor (arr) {
    this.arr = arr
    this.indexMap = this.makeIndexMap(arr)
  }

  makeIndexMap (arr) {
    return arr.reduce((acc, item, index) => {
      acc[item.key] = index
      return acc
    }, {})
  }

  push (obj) {
    this.arr.push(obj)
    this.indexMap[obj.key] = this.arr.length - 1
  }

  delete (key) {
    const index = this.indexMap[key]

    this.arr.splice(index, 1)
    this.indexMap = this.makeIndexMap(this.arr)
  }

  get (key) {
    return this.arr[this.getIndex(key)]
  }

  getIndex (key) {
    return this.indexMap[key]
  }

  getArr () {
    return this.arr
  }
}

/*
 * Copyright (c) 2020. Alexander Sorokin
 */

import ViTable from './ViTable'
import ViTableTh from './ViTableTh'

export default ViTable
export { ViTableTh }

/*
 * Copyright (c) 2020. Alexander Sorokin
 */

import { mount } from '@vue/test-utils'
import ViTable, { ViTableTh } from '@/components/ViTable'
import Vue from 'vue'

describe('ViTable.vue', () => {
  it('sorting', async () => {
    const data = Vue.observable({
      headers: [
        { text: 'ID', key: 'id' }
      ],
      items: Vue.observable([{ id: 2 }, { id: 1 }, { id: 3 }])
    })
    const wrapper = mount(ViTable, {
      propsData: data
    })

    expect(wrapper.find('td').text()).toBe('2')

    wrapper.findComponent(ViTableTh).trigger('click')
    await Vue.nextTick()
    expect(wrapper.find('td').text()).toBe('1')

    wrapper.findComponent(ViTableTh).trigger('click')
    await Vue.nextTick()
    expect(wrapper.find('td').text()).toBe('3')

    wrapper.findComponent(ViTableTh).trigger('click')
    await Vue.nextTick()
    expect(wrapper.find('td').text()).toBe('2')
  })

  it('multisorting', async () => {
    const data = Vue.observable({
      headers: [
        { text: 'name', key: 'name' },
        { text: 'surname', key: 'surname' }
      ],
      multisort: true,
      items: Vue.observable([
        {
          name: 'a',
          surname: 'b'
        },
        {
          name: 'b',
          surname: 'c'
        },
        {
          name: 'b',
          surname: 'a'
        }
      ])
    })

    const wrapper = mount(ViTable, {
      propsData: data
    })

    const ths = wrapper.findAll('th')
    ths.at(0).trigger('click')
    await Vue.nextTick()
    ths.at(1).trigger('click')
    await Vue.nextTick()

    let firstRowTds = wrapper.find('tr').findAll('td')
    expect(firstRowTds.at(0).text()).toBe('a')
    expect(firstRowTds.at(1).text()).toBe('b')

    ths.at(1).trigger('click')
    await Vue.nextTick()

    firstRowTds = wrapper.find('tr').findAll('td')
    expect(firstRowTds.at(0).text()).toBe('a')
    expect(firstRowTds.at(1).text()).toBe('b')
  })

  it('pagination', () => {
    const data = Vue.observable({
      headers: [
        { text: 'ID', key: 'id' }
      ],
      items: Vue.observable([{ id: 1 }, { id: 2 }, { id: 3 }]),
      itemsPerPage: 2
    })
    const wrapper = mount(ViTable, {
      propsData: data
    })

    expect(wrapper.findAll('tr')).toHaveLength(2)
  })

  it('filter', async () => {
    const data = Vue.observable({
      headers: [
        { text: 'ID', key: 'id' },
        { text: 'name', key: 'name' }
      ],
      items: Vue.observable([
        {
          id: 1,
          name: 'Alice'
        },
        {
          id: 2,
          name: 'Bob'
        },
        {
          id: 3,
          name: 'Alexander'
        }
      ])
    })

    const wrapper = mount(ViTable, {
      propsData: data
    })

    const inputWrapper = wrapper.get('input')
    const formWrapper = wrapper.get('form')

    inputWrapper.setValue('1')

    await Vue.nextTick()

    formWrapper.trigger('submit')

    await Vue.nextTick()

    expect(wrapper.findAll('tr')).toHaveLength(1)

    inputWrapper.setValue('al')

    await Vue.nextTick()

    formWrapper.trigger('submit')

    await Vue.nextTick()

    expect(wrapper.findAll('tr')).toHaveLength(2)
  })

  it('wrong page is taken into account', async () => {
    const data = Vue.observable({
      headers: [
        { text: 'ID', key: 'id' }
      ],
      items: Vue.observable([{ id: 1 }]),
      page: 2,
      itemsPerPage: 1
    })
    const wrapper = mount(ViTable, {
      propsData: data
    })

    expect(wrapper.vm.selfPage).toBe(1)
  })

  it('letter case is not taken into account when sorting', async () => {
    const data = Vue.observable({
      headers: [
        { text: 'Name', key: 'name' }
      ],
      items: Vue.observable([{ name: 'J' }, { name: 'a' }])
    })
    const wrapper = mount(ViTable, {
      propsData: data
    })

    wrapper.findComponent(ViTableTh).trigger('click')

    await Vue.nextTick()

    const tds = wrapper.findAll('td')

    expect(tds.at(0).text()).toBe('a')
    expect(tds.at(1).text()).toBe('J')
  })
})
